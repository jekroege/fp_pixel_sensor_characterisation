[Timepix3_0]
type = "Timepix3"
number_of_pixels = 256,256
pixel_pitch = 55um,55um
position = 0mm,0mm,0mm
orientation = 9deg,189deg,0deg
orientation_mode = "xyz"
spatial_resolution = 4um, 4um
time_resolution = 1.56ns

# Add all other detectors here:
# -> Timepix3_(1-6) and ATLASpix_0

# Tip:
# For the spatial resolution of the ATLASpix_0,
# assume a binary resolution (see manual)